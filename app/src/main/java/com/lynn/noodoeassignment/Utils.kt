package com.lynn.noodoeassignment

import java.util.*
import java.util.concurrent.TimeUnit

fun getTimeZoneOffset() = TimeUnit.HOURS.convert(TimeZone.getDefault().rawOffset.toLong(), TimeUnit.MILLISECONDS).toInt()