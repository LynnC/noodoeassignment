package com.lynn.noodoeassignment

import android.app.Application
import com.lynn.noodoeassignment.data.api.BASE_URL
import com.lynn.noodoeassignment.di.*

class MyApp : Application() {

    private val appComponent: AppComponent = DaggerAppComponent.builder()
        .appModule(AppModule())
        .build()!!

    val networkComponent: NetworkComponent = DaggerNetworkComponent.builder()
        .appComponent(appComponent)
        .networkModule(NetworkModule(BASE_URL))
        .build()!!
}