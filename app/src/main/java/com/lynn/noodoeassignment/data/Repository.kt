package com.lynn.noodoeassignment.data

import com.lynn.noodoeassignment.data.model.LoggedInUser
import com.lynn.noodoeassignment.data.request.UpdateUserRequest
import com.lynn.noodoeassignment.data.response.LoginResponse
import com.lynn.noodoeassignment.data.response.UpdateUserResponse
import io.reactivex.Single
import java.lang.NullPointerException
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Class that requests authentication and user information from the remote data source and
 * maintains an in-memory cache of login status and user credentials information.
 */
class Repository @Inject constructor(private val remoteDataSource: DataSource) {

    // in-memory cache of the loggedInUser object
    private var user: LoggedInUser? = null

    private val isLoggedIn: Boolean
        get() = user != null

    init {
        // If user credentials will be cached in local storage, it is recommended it be encrypted
        // @see https://developer.android.com/training/articles/keystore
        user = null
    }

    fun login(username: String, password: String): Single<LoggedInUser> {
        return remoteDataSource.login(username, password).map { convertToLoggedInUser(it) }
    }

    private fun convertToLoggedInUser(loginResponse: LoginResponse): LoggedInUser {
        user = LoggedInUser(
            userId = loginResponse.objectId,
            displayName = loginResponse.username,
            token = loginResponse.sessionToken
        )
        return user as LoggedInUser
    }

    fun updateTimezone(timezone: Int): Single<UpdateUserResponse> {
        return if (isLoggedIn) remoteDataSource.updateUser(user!!, UpdateUserRequest(timezone))
        else Single.error(NullPointerException())
    }
}
