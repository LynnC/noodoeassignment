package com.lynn.noodoeassignment.data.api

import com.lynn.noodoeassignment.data.request.UpdateUserRequest
import com.lynn.noodoeassignment.data.response.LoginResponse
import com.lynn.noodoeassignment.data.response.UpdateUserResponse
import io.reactivex.Single
import retrofit2.http.*


const val BASE_URL = "https://watch-master-staging.herokuapp.com/"

interface NoodoeApi {

    /**
    curl -X GET \
    -H "X-Parse-Application-Id: ${APPLICATION_ID}" \
    -H "X-Parse-REST-API-Key: ${REST_API_KEY}" \
    -H "X-Parse-Revocable-Session: 1" \
    -G \
    --data-urlencode 'username=cooldude6' \
    --data-urlencode 'password=p_n7!-e8' \
    https://YOUR.PARSE-SERVER.HERE/parse/login
     */
    @Headers(
        "X-Parse-Application-Id: vqYuKPOkLQLYHhk4QTGsGKFwATT4mBIGREI2m8eD",
        "X-Parse-REST-API-Key: \"\""
    )
    @GET("api/login")
    fun login(
        @Query("username") username: String,
        @Query("password") password: String
    ): Single<LoginResponse>


    @Headers(
        "X-Parse-Application-Id: vqYuKPOkLQLYHhk4QTGsGKFwATT4mBIGREI2m8eD",
        "X-Parse-REST-API-Key: \"\"",
        "Content-Type: application/json"
    )
    @PUT("api/users/{objectId}")
    fun updateUser(
        @Header("X-Parse-Session-Token") token: String,
        @Path("objectId") objectId: String,
        @Body updateUserRequest: UpdateUserRequest
    ): Single<UpdateUserResponse>

}