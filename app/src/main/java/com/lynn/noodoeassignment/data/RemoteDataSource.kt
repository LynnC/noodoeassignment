package com.lynn.noodoeassignment.data

import com.lynn.noodoeassignment.data.api.NoodoeApi
import com.lynn.noodoeassignment.data.model.LoggedInUser
import com.lynn.noodoeassignment.data.request.UpdateUserRequest
import com.lynn.noodoeassignment.data.response.LoginResponse
import com.lynn.noodoeassignment.data.response.UpdateUserResponse
import io.reactivex.Single
import javax.inject.Inject

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
class RemoteDataSource @Inject constructor(private val noodoeApi: NoodoeApi) : DataSource {

    override fun login(username: String, password: String): Single<LoginResponse> {

        return noodoeApi.login(username, password)
    }

    override fun updateUser(user: LoggedInUser, updateUserRequest: UpdateUserRequest): Single<UpdateUserResponse> {

        return noodoeApi.updateUser(user.token, user.userId, updateUserRequest)
    }

}

