package com.lynn.noodoeassignment.data

import com.lynn.noodoeassignment.data.model.LoggedInUser
import com.lynn.noodoeassignment.data.request.UpdateUserRequest
import com.lynn.noodoeassignment.data.response.LoginResponse
import com.lynn.noodoeassignment.data.response.UpdateUserResponse
import io.reactivex.Single

interface DataSource {

    fun login(username: String, password: String): Single<LoginResponse>

    fun updateUser(user: LoggedInUser, updateUserRequest: UpdateUserRequest): Single<UpdateUserResponse>
}