package com.lynn.noodoeassignment.data.request

data class UpdateUserRequest(val timezone: Int)