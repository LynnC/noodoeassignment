package com.lynn.noodoeassignment.data.response

import com.google.gson.annotations.SerializedName

/**
{
    "objectId": "WkuKfCAdGq",
    "username": "test2@qq.com",
    "code": "4wtmah5h",
    "isVerifiedReportEmail": true,
    "reportEmail": "test2@qq.com",
    "createdAt": "2019-07-12T07:07:18.027Z",
    "updatedAt": "2019-08-21T08:14:33.883Z",
    "timezone": 5,
    "ACL": {
        "WkuKfCAdGq": {
            "read": true,
            "write": true
        }
    },
    "sessionToken": "r:3840bdf10608c8fa25114dd1b8996d2a"
}
*/

data class LoginResponse(
    val objectId: String,
    val username: String,
    val code: String,
    val isVerifiedReportEmail: Boolean,
    val reportEmail: String,
    val createdAt: String,
    val updatedAt: String,
    val timezone: Int,
    @SerializedName(value = "ACL")
    val acl: Map<String, Permission>,
    val sessionToken: String
)

data class Permission(val read: Boolean, val write: Boolean)