package com.lynn.noodoeassignment.data.response

/**
{
"updatedAt": "2019-08-24T13:46:57.329Z",
"role": {
"__op": "Delete"
}
}
 */
data class UpdateUserResponse(val updatedAt: String)