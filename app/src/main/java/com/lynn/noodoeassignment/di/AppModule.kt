package com.lynn.noodoeassignment.di

import com.lynn.kotlintest.scheduler.BaseSchedulerProvider
import com.lynn.kotlintest.scheduler.SchedulerProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Singleton
    @Provides
    fun provideSchedulerProvider(): BaseSchedulerProvider = SchedulerProvider()

}