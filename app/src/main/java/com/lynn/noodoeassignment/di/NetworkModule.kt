package com.lynn.noodoeassignment.di

import android.util.Log
import com.lynn.noodoeassignment.data.DataSource
import com.lynn.noodoeassignment.data.RemoteDataSource
import com.lynn.noodoeassignment.data.api.NoodoeApi
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@Module
class NetworkModule(private val baseUrl: String) {

    @RemoteDataScope
    @Provides
    fun provideOkHttpClient(): OkHttpClient {
        val logger = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger {
            Log.d("Noodoe_API", it)
        })
        logger.level = HttpLoggingInterceptor.Level.BASIC

        return OkHttpClient.Builder()
            .addInterceptor(logger)
            .build()
    }

    @RemoteDataScope
    @Provides
    fun provideNoodoeApi(client: OkHttpClient): NoodoeApi = Retrofit.Builder()
        .baseUrl(baseUrl)
        .client(client)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
        .create(NoodoeApi::class.java)

    @RemoteDataScope
    @Provides
    fun provideDataSource(noodoeApi: NoodoeApi): DataSource = RemoteDataSource(noodoeApi)

}
