package com.lynn.noodoeassignment.di

import javax.inject.Scope

@Scope
@Retention
annotation class RemoteDataScope
