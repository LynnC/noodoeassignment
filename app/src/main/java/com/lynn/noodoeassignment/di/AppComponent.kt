package com.lynn.noodoeassignment.di

import com.lynn.kotlintest.scheduler.BaseSchedulerProvider
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {

    fun schedulerProvider(): BaseSchedulerProvider
}