package com.lynn.noodoeassignment.di

import com.lynn.noodoeassignment.ui.login.LoginViewModel
import dagger.Component

@RemoteDataScope
@Component(
    dependencies = [AppComponent::class],
    modules = [NetworkModule::class]
)
interface NetworkComponent {

    fun inject(loginViewModel: LoginViewModel)

}