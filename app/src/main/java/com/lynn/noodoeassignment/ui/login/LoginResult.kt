package com.lynn.noodoeassignment.ui.login

/**
 * Authentication result : login success (user details), update success (timezone) or error message.
 */
data class LoginResult(
    val loginSuccess: LoggedInUserView? = null,
    val updateSuccess: Boolean? = null,
    val error: Int? = null
)
