package com.lynn.noodoeassignment.ui.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import android.util.Patterns
import com.lynn.kotlintest.scheduler.BaseSchedulerProvider
import com.lynn.noodoeassignment.MyApp
import com.lynn.noodoeassignment.data.Repository

import com.lynn.noodoeassignment.R
import com.lynn.noodoeassignment.getTimeZoneOffset
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class LoginViewModel(myApp: MyApp) : ViewModel() {

    @Inject
    lateinit var repository: Repository

    @Inject
    lateinit var schedulerProvider: BaseSchedulerProvider

    private val compositeDisposable = CompositeDisposable()

    private val _loginForm = MutableLiveData<LoginFormState>()
    val loginFormState: LiveData<LoginFormState> = _loginForm

    private val _loginResult = MutableLiveData<LoginResult>()
    val loginResult: LiveData<LoginResult> = _loginResult

    init {
        myApp.networkComponent.inject(this)
    }

    fun login(username: String, password: String) {
        // can be launched in a separate asynchronous job
        val disposable = repository.login(username, password)
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .subscribe(
                {
                    _loginResult.value =
                        LoginResult(loginSuccess = LoggedInUserView(userId = it.userId, displayName = it.displayName))
                }
            ) {
                _loginResult.value = LoginResult(error = R.string.login_failed)
            }

        compositeDisposable.add(disposable)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    fun loginDataChanged(username: String, password: String) {
        if (!isUserNameValid(username)) {
            _loginForm.value = LoginFormState(usernameError = R.string.invalid_username)
        } else if (!isPasswordValid(password)) {
            _loginForm.value = LoginFormState(passwordError = R.string.invalid_password)
        } else {
            _loginForm.value = LoginFormState(isDataValid = true)
        }
    }

    // A placeholder username validation check
    private fun isUserNameValid(username: String): Boolean {
        return if (username.contains('@')) {
            Patterns.EMAIL_ADDRESS.matcher(username).matches()
        } else {
            username.isNotBlank()
        }
    }

    // A placeholder password validation check
    private fun isPasswordValid(password: String): Boolean {
        return password.length > 5
    }

    fun updateTimeZone() {
        val disposable = repository.updateTimezone(getTimeZoneOffset())
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .subscribe({
                _loginResult.value = LoginResult(
                    updateSuccess = true
                )
            }) {
                _loginResult.value = LoginResult(
                    updateSuccess = false
                )
            }

        compositeDisposable.add(disposable)
    }
}
