作業要求∶
=======================================================
 
Write an Android App with Kotlin, RxJava and Retrofit for case 1 and case 2.
 
=======================================================
 
 
Case 1: Login API 
 
請依照文件的說明，執行使用者登入
 
http://docs.parseplatform.org/rest/guide/#logging-in
 
 
 
測試環境的資料:
 
API URL:
 
   https://watch-master-staging.herokuapp.com/api/login
 
API Params:
 
"X-Parse-Application-Id": "vqYuKPOkLQLYHhk4QTGsGKFwATT4mBIGREI2m8eD"
 
"X-Parse-REST-API-Key": ""
 
username: "test2@qq.com"
 
password: "test1234qq"
 
 
 
Case 2: Updating users API 
 
請依照文件的說明，以及先前API得到的回應參數，修改目前使用者的timezone資料。
 
http://docs.parseplatform.org/rest/guide/#updating-users
 
 
 
測試環境的資料:
 
API URL:
 
   https://watch-master-staging.herokuapp.com/api/users/{previous API's objectId}
 
API Params:
 
  "X-Parse-Application-Id": "vqYuKPOkLQLYHhk4QTGsGKFwATT4mBIGREI2m8eD"
 
  "X-Parse-REST-API-Key": ""
 
  "X-Parse-Session-Token": "{previous API's sessionToken}"
 
  "timezone": 8
